keycloak
========

#### links
- https://github.com/keycloak/keycloak-nodejs-admin-client
- https://github.com/codecentric/helm-charts/tree/master/charts/keycloak
- https://github.com/keycloak/keycloak
- https://daenney.github.io/2018/10/30/beyondcorp-at-home-authz
- https://daenney.github.io/2018/10/27/beyondcorp-at-home
- https://kazuhira-r.hatenablog.com/entry/2019/02/23/194727
- https://github.com/keycloak/keycloak-gatekeeper
- https://github.com/keycloak/keycloak-gatekeeper/releases
- https://hub.docker.com/r/keycloak/keycloak-gatekeeper
- https://github.com/bitnami/bitnami-docker-keycloak-gatekeeper
- https://raw.githubusercontent.com/keycloak/keycloak-gatekeeper/master/config_sample.yml
- https://www.keycloak.org/docs/latest/securing_apps/index.html#_keycloak_generic_adapter
- https://wiredcraft.com/blog/securing-components-in-a-microservice-context/
- https://www.keycloak.org/docs/latest/
- https://www.keycloak.org/docs-api/6.0/rest-api/index.html
- https://dev.to/karlredman/keycloak-v5-gatekeeper-v5-flowcharts-easily-create-and-restrict-an-isolated-iodc-client-service-by-group-role-53h4


```bash
# create certs for nginx
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout key -out crt

docker-compose up -d keycloak

# get token for provision
export TKN=$(curl -X POST 'http://127.0.0.1:8080/auth/realms/master/protocol/openid-connect/token' \
 -d "username=admin" \
 -d 'password=admin' \
 -d 'grant_type=password' \
 -d 'client_id=admin-cli' | jq -r '.access_token')

echo $TKN # 60s expire

# get realms
curl -H "Authorization: bearer $TKN" http://127.0.0.1:8080/auth/admin/realms | jq

# create realm
curl -v -X POST \
  -H "Authorization: bearer $TKN" \
  -H "Content-Type: application/json" \
  -d @realm1.json \
  http://127.0.0.1:8080/auth/admin/realms | jq

# get clients
curl -H "Authorization: bearer $TKN" \
  http://127.0.0.1:8080/auth/admin/realms/realm1/clients | jq

# create client in realm
# client1.json
# "bearerOnly": false
# "publicClient": false
# for confidential

curl -v -X POST \
  -H "Authorization: bearer $TKN" \
  -H "Content-Type: application/json" \
  -d @client1.json \
  http://127.0.0.1:8080/auth/admin/realms/realm1/clients | jq

curl -H "Authorization: bearer $TKN" \
  http://127.0.0.1:8080/auth/admin/realms/realm1/clients | jq

# create user role
curl -v -X POST \
  -H "Authorization: bearer $TKN" \
  -H "Content-Type: application/json" \
  -d @role1.json \
  http://127.0.0.1:8080/auth/admin/realms/realm1/clients/client1/roles | jq

# create admin role
curl -v -X POST \
  -H "Authorization: bearer $TKN" \
  -H "Content-Type: application/json" \
  -d @role2.json \
  http://127.0.0.1:8080/auth/admin/realms/realm1/clients/client1/roles | jq

# get roles
curl -v -X GET \
  -H "Authorization: bearer $TKN" \
  -H "Content-Type: application/json" \
  http://127.0.0.1:8080/auth/admin/realms/realm1/clients/client1/roles | jq

# get client secret
curl -v -X GET \
  -H "Authorization: bearer $TKN" \
  -H "Content-Type: application/json" \
  http://127.0.0.1:8080/auth/admin/realms/realm1/clients/client1/client-secret | jq

# create user
curl -v -X POST \
  -H "Authorization: bearer $TKN" \
  -H "Content-Type: application/json" \
  -d @user.json \
  http://127.0.0.1:8080/auth/admin/realms/realm1/users | jq

# get users
curl -v -X GET \
  -H "Authorization: bearer $TKN" \
  -H "Content-Type: application/json" \
  http://127.0.0.1:8080/auth/admin/realms/realm1/users | jq

# fix unable to find the client_id in the aud claim
# client_id is not in the audience anymore
# add the audience protocol mapper to the client with the audience pointed to the client_id

curl -v -X POST \
  -H "Authorization: bearer $TKN" \
  -H "Content-Type: application/json" \
  -d @protocol_mapper.json \
  http://127.0.0.1:8080/auth/admin/realms/realm1/client-scopes/client1/protocol-mappers/models | jq

# get protocol mappers
curl -v -X GET \
  -H "Authorization: bearer $TKN" \
  -H "Content-Type: application/json" \
  http://127.0.0.1:8080/auth/admin/realms/realm1/client-scopes/client1/protocol-mappers/models | jq
```

