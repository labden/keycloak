# get token for provision
export TKN=$(curl -X POST 'http://127.0.0.1:8080/auth/realms/master/protocol/openid-connect/token' \
 -d "username=admin" \
 -d 'password=admin' \
 -d 'grant_type=password' \
 -d 'client_id=admin-cli' | jq -r '.access_token')

echo $TKN # 60s expire

# get realms
curl -H "Authorization: bearer $TKN" http://127.0.0.1:8080/auth/admin/realms | jq

# create realm
curl -v -X POST \
  -H "Authorization: bearer $TKN" \
  -H "Content-Type: application/json" \
  -d @realm1.json \
  http://127.0.0.1:8080/auth/admin/realms | jq

# get clients
curl -H "Authorization: bearer $TKN" \
  http://127.0.0.1:8080/auth/admin/realms/realm1/clients | jq

# create client in realm
# client1.json
# "bearerOnly": false
# "publicClient": false
# for confidential

curl -v -X POST \
  -H "Authorization: bearer $TKN" \
  -H "Content-Type: application/json" \
  -d @client1.json \
  http://127.0.0.1:8080/auth/admin/realms/realm1/clients | jq

curl -H "Authorization: bearer $TKN" \
  http://127.0.0.1:8080/auth/admin/realms/realm1/clients | jq

# create user role
curl -v -X POST \
  -H "Authorization: bearer $TKN" \
  -H "Content-Type: application/json" \
  -d @role1.json \
  http://127.0.0.1:8080/auth/admin/realms/realm1/clients/client1/roles | jq

# create admin role
curl -v -X POST \
  -H "Authorization: bearer $TKN" \
  -H "Content-Type: application/json" \
  -d @role2.json \
  http://127.0.0.1:8080/auth/admin/realms/realm1/clients/client1/roles | jq

# get roles
curl -v -X GET \
  -H "Authorization: bearer $TKN" \
  -H "Content-Type: application/json" \
  http://127.0.0.1:8080/auth/admin/realms/realm1/clients/client1/roles | jq

# get client secret
curl -v -X GET \
  -H "Authorization: bearer $TKN" \
  -H "Content-Type: application/json" \
  http://127.0.0.1:8080/auth/admin/realms/realm1/clients/client1/client-secret | jq

# create user
curl -v -X POST \
  -H "Authorization: bearer $TKN" \
  -H "Content-Type: application/json" \
  -d @user.json \
  http://127.0.0.1:8080/auth/admin/realms/realm1/users | jq

# get users
curl -v -X GET \
  -H "Authorization: bearer $TKN" \
  -H "Content-Type: application/json" \
  http://127.0.0.1:8080/auth/admin/realms/realm1/users | jq

# fix unable to find the client_id in the aud claim
# client_id is not in the audience anymore
# add the audience protocol mapper to the client with the audience pointed to the client_id

curl -v -X POST \
  -H "Authorization: bearer $TKN" \
  -H "Content-Type: application/json" \
  -d @protocol_mapper.json \
  http://127.0.0.1:8080/auth/admin/realms/realm1/client-scopes/client1/protocol-mappers/models | jq

# get protocol mappers
curl -v -X GET \
  -H "Authorization: bearer $TKN" \
  -H "Content-Type: application/json" \
  http://127.0.0.1:8080/auth/admin/realms/realm1/client-scopes/client1/protocol-mappers/models | jq

