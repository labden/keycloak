import cryptography, jwt
from http.server import HTTPServer, BaseHTTPRequestHandler


class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        if self.path == '/':
            self.send_response(200)
            self.end_headers()
            self.wfile.write(b'public')
            # print(self.headers)
        elif self.path == '/user':
            self.send_response(200)
            self.end_headers()
            self.wfile.write(b'user')
            # print(self.headers)
            print(jwt.decode(self.headers['X-Auth-Token'], verify=False))

        elif self.path == '/admin':
            self.send_response(200)
            self.end_headers()
            self.wfile.write(b'admin')
        else:
            self.send_response(404)
            self.end_headers()
            self.wfile.write(b'404')


httpd = HTTPServer(('0.0.0.0', 9000), SimpleHTTPRequestHandler)
httpd.serve_forever()
