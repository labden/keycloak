FROM python:3.7-alpine
COPY app.py /app.py
COPY requirements.txt /requirements.txt

RUN apk add --no-cache \
        gcc \
        libressl-dev \
        musl-dev \
        libffi-dev && \
    pip install --upgrade pip && \
    pip install --no-cache-dir -r /requirements.txt && \
    apk del \
        gcc \
        libressl-dev \
        musl-dev \
        libffi-dev

EXPOSE 9000
ENTRYPOINT ["python", "-u", "/app.py"]
