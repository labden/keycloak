# get token
export TKN=$(curl -X POST 'http://127.0.0.1:8080/auth/realms/master/protocol/openid-connect/token' \
 -d "username=admin" \
 -d 'password=admin' \
 -d 'grant_type=password' \
 -d 'client_id=admin-cli' | jq -r '.access_token')

# get client-secret
export SECRET=$(curl -v -X GET \
  -H "Authorization: bearer $TKN" \
  -H "Content-Type: application/json" \
  http://127.0.0.1:8080/auth/admin/realms/realm1/clients/client1/client-secret | jq -r '.value')

# update client-secret in gatekeeper.yml
LINE="client-secret: $SECRET"
sed -i "s/^.*secret.*$/${LINE}/" gatekeeper.yml

